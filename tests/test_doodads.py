from source.doodads import get_upper, paths

class TestDoodads():

    def test_upper(self):
        word = 'hello'
        upper = get_upper(word)
        assert upper == 'HELLO', f'Expecting {upper} to be HELLO'

    def test_paths(self):
        x = paths(0)
        assert x == 0, f'Expecting {x} to be 0'

    def test_paths(self):
        x = paths(1)
        assert x == 1, f'Expecting {x} to be 1'

    def test_paths(self):
        x = paths(2)
        assert x == 2, f'Expecting {x} to be 2'

    def test_paths(self):
        x = paths(3)
        assert x == 3, f'Expecting {x} to be 3'

    def test_paths(self):
        x = paths(4)
        assert x == 4, f'Expecting {x} to be 4'

from source.math import add, multiply, subtract

class TestMath():

    def test_add(self):
        sum = add(4, 5)
        assert sum == 9, f'Expecting sum to be 9 but was {sum}'

    def test_add_2(self):
        sum = add(5, 4)
        assert sum == 9, f'Expecting sum to be 9 but was {sum}'

    def test_multiply(self):
        product = multiply(4, 5)
        assert product == 20, f'Expecting product to be 20 but was {product}'

    def test_subtract(self):
        answer = subtract(5, 4)
        assert answer == 1, f'Expecting answer to be 1 but was {answer}'

from source.doodads2 import dumb_dumb, ultimate_doodad

class TestDoodads2():

    def test_ultimate_doodad_0(self):
        x = ultimate_doodad(0)
        assert x == 0

    def test_ultimate_doodad_1(self):
        x = ultimate_doodad(1)
        assert x == 1

    def test_ultimate_doodad_2(self):
        x = ultimate_doodad(2)
        assert x == 2

    def test_dumb_dumb(self):
        x = dumb_dumb(5)
        assert x == 5
